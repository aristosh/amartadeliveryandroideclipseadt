package com.monoceros.amartadelivery.helpers;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;

import android.app.Fragment;
import android.location.Location;
import android.os.Bundle;
import android.view.View;

public class LocationListFragment extends Fragment implements
			GooglePlayServicesClient.ConnectionCallbacks,
			GooglePlayServicesClient.OnConnectionFailedListener,
			LocationListener {
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		// Create the LocationRequest object
	    mLocationRequest = LocationRequest.create();
	    // Use high accuracy
	    //mLocationRequest.setPriority( LocationRequest.PRIORITY_NO_POWER );
	    mLocationRequest.setPriority( LocationRequest.PRIORITY_HIGH_ACCURACY );
	    // Set the update interval to 5 seconds
	    mLocationRequest.setInterval(5);
	    // Set the fastest update interval to 1 second
	    mLocationRequest.setFastestInterval(1);
	    
		mLocationClient = new LocationClient(getActivity(), this, this);
	}
	@Override
	public void onStart() {
		super.onStart();
		
	}
	
	@Override
	public void onStop() {
		if (mLocationClient.isConnected()) {
        	mLocationClient.removeLocationUpdates(this);
        }
        mLocationClient.disconnect();
		super.onStop();
	}

	@Override
	public void onLocationChanged(Location location) {}

	@Override
	public void onConnectionFailed(ConnectionResult arg0) {}

	@Override
	public void onConnected(Bundle arg0) {
		mLocationClient.requestLocationUpdates(mLocationRequest, this);
	}
	
	@Override
	public void onDisconnected() {}
	
	public Location getLastLocation() {
		return mLocationClient.getLastLocation();
	}
	
	public LocationClient getLocationClient() {
		return mLocationClient;
	}
	
	private LocationClient mLocationClient;
	private LocationRequest mLocationRequest;
}
