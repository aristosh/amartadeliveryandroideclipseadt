package com.monoceros.amartadelivery.helpers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SQLHelper extends SQLiteOpenHelper {
	public static final int DATABASE_VERSION = 12;
    public static final String DATABASE_NAME = "AmartaDelivery.db";
    
	public SQLHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}
    
	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(SQLSchemaHelper.DeliveryOrderSQL.SQL_CREATE_ENTRIES);
		db.execSQL(SQLSchemaHelper.DeliveryItemSQL.SQL_CREATE_ENTRIES);
		db.execSQL(SQLSchemaHelper.PhotoSQL.SQL_CREATE_ENTRIES);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL(SQLSchemaHelper.DeliveryOrderSQL.SQL_DELETE_ENTRIES);
		db.execSQL(SQLSchemaHelper.DeliveryItemSQL.SQL_DELETE_ENTRIES);
		db.execSQL(SQLSchemaHelper.PhotoSQL.SQL_DELETE_ENTRIES);
        onCreate(db);
	}	
}
