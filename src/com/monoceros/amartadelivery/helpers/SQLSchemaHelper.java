package com.monoceros.amartadelivery.helpers;

import android.provider.BaseColumns;

public final class SQLSchemaHelper {
	private static final String INT_TYPE      = " INTEGER";
    private static final String TEXT_TYPE     = " TEXT";
    private static final String DEF_NOW 	  = " DEFAULT CURRENT_TIMESTAMP";
    private static final String COMMA_SEP     = ",";
    
	public SQLSchemaHelper() {}
	
	public static abstract class DeliveryOrderSQL implements BaseColumns {
		public final static String TABLE = "t_delivery_order";
		
		public final static String COLUMN_ID_DEALER = "id_dealer";
		public final static String COLUMN_DEALER_NAME = "dealer_name";
		public final static String COLUMN_NO_DO = "no_do";
		public final static String COLUMN_DO_DATE = "do_date";
		public final static String COLUMN_DRIVER_NAME = "driver_name";
		public final static String COLUMN_NO_P = "no_p";
		public final static String COLUMN_SHIFT = "shift";
		public final static String COLUMN_ITEMS = "rows";
		public final static String COLUMN_TIMESTAMP = "timestamp";
	    
	    public static final String SQL_CREATE_ENTRIES =
	        "CREATE TABLE " + TABLE + " (" +
	        	_ID 					+ INT_TYPE 		+ " PRIMARY KEY," +
	        	_COUNT 					+ INT_TYPE 		+ COMMA_SEP +
		        COLUMN_ID_DEALER 		+ TEXT_TYPE 	+ COMMA_SEP +
		        COLUMN_DEALER_NAME 		+ TEXT_TYPE 	+ COMMA_SEP +
		        COLUMN_NO_DO 			+ TEXT_TYPE 	+ COMMA_SEP +
		        COLUMN_DO_DATE 			+ TEXT_TYPE 	+ COMMA_SEP +
		        COLUMN_DRIVER_NAME		+ TEXT_TYPE 	+ COMMA_SEP +
		        COLUMN_NO_P      		+ TEXT_TYPE 	+ COMMA_SEP +
		        COLUMN_SHIFT       		+ TEXT_TYPE 	+ 
	        " )";
	    
	    public static final String SQL_DELETE_ENTRIES =
	    	"DROP TABLE IF EXISTS " + TABLE;
	}
	
	
	public static abstract class DeliveryItemSQL implements BaseColumns {
		public final static String TABLE = "t_delivery_item";
		
		public final static String COLUMN_NO_DO = DeliveryOrderSQL.COLUMN_NO_DO;
		
		public final static String COLUMN_NO_SPK = "no_spk";
		public final static String COLUMN_SPK_DATE = "spk_date";
		public final static String COLUMN_NO_SJ = "no_sj";			// conside this as PK
		public final static String COLUMN_SJ_DATE = "sj_date";
		public final static String COLUMN_KONSUMEN_NAME = "konsumen_name";
		public final static String COLUMN_KONSUMEN_ADRS = "konsumen_adrs";
		public final static String COLUMN_KONSUMEN_TELP = "konsumen_telp";
		public final static String COLUMN_ID_TIPE_MOTOR = "id_tipe_motor";
		public final static String COLUMN_JENIS_MOTOR = "jenis_motor";
		public final static String COLUMN_WARNA_MOTOR = "warna_motor";
		public final static String COLUMN_NO_MESIN = "no_mesin";
		public final static String COLUMN_NO_RANGKA = "no_rangka";
		public final static String COLUMN_TAHUN = "tahun";
		public final static String COLUMN_AUTOGRAPH = "autograph";
		public final static String COLUMN_LATITUDE = "latitude";
		public final static String COLUMN_LONGITUDE = "longitude";
		public final static String COLUMN_AUTOGRAPH_TIMESTAMP = "autograph_timestamp";
		
	    public static final String SQL_CREATE_ENTRIES =
	        "CREATE TABLE " + TABLE + " (" +
	        	_ID 						+ INT_TYPE 	+ " PRIMARY KEY," +
	        	_COUNT 						+ INT_TYPE 	+ COMMA_SEP +
	        	
	        	COLUMN_NO_DO 				+ TEXT_TYPE + COMMA_SEP +
	        	
		        COLUMN_NO_SPK 				+ TEXT_TYPE + COMMA_SEP +
		        COLUMN_SPK_DATE 			+ TEXT_TYPE + COMMA_SEP +
		        COLUMN_NO_SJ 				+ TEXT_TYPE + COMMA_SEP +
		        COLUMN_SJ_DATE 				+ TEXT_TYPE + COMMA_SEP +
		        COLUMN_KONSUMEN_NAME 		+ TEXT_TYPE + COMMA_SEP +
		        COLUMN_KONSUMEN_ADRS 		+ TEXT_TYPE + COMMA_SEP +
		        COLUMN_KONSUMEN_TELP 		+ TEXT_TYPE + COMMA_SEP +
		        COLUMN_ID_TIPE_MOTOR 		+ TEXT_TYPE + COMMA_SEP +
		        COLUMN_JENIS_MOTOR 			+ TEXT_TYPE + COMMA_SEP +
		        COLUMN_WARNA_MOTOR 			+ TEXT_TYPE + COMMA_SEP +
		        COLUMN_NO_MESIN 			+ TEXT_TYPE + COMMA_SEP +
		        COLUMN_NO_RANGKA 			+ TEXT_TYPE + COMMA_SEP +
		        COLUMN_TAHUN 				+ TEXT_TYPE + COMMA_SEP +
		        COLUMN_AUTOGRAPH			+ TEXT_TYPE + COMMA_SEP +
		        COLUMN_LATITUDE				+ TEXT_TYPE + COMMA_SEP +
		        COLUMN_LONGITUDE			+ TEXT_TYPE + COMMA_SEP +
		        COLUMN_AUTOGRAPH_TIMESTAMP 	+ TEXT_TYPE + COMMA_SEP +
		        
		        "FOREIGN KEY (" + COLUMN_NO_DO + ") REFERENCES " + 
	        	DeliveryOrderSQL.TABLE + "(" + DeliveryOrderSQL.COLUMN_NO_DO + ") ON DELETE CASCADE " +
	        " )";
	    
		public static final String SQL_DELETE_ENTRIES =
		    	"DROP TABLE IF EXISTS " + TABLE;
	}
	
	public static abstract class PhotoSQL implements BaseColumns {
		public final static String TABLE = "t_photo";
		
		public final static String COLUMN_NO_SJ = DeliveryItemSQL.COLUMN_NO_SJ;
		
		public final static String COLUMN_PHOTO_PATH = "photo_path";
		public final static String COLUMN_LATITUDE = "latitude";
		public final static String COLUMN_LONGITUDE = "longitude";
		public final static String COLUMN_TIMESTAMP = "timestamp";
		
	    public static final String SQL_CREATE_ENTRIES =
	        "CREATE TABLE " + TABLE + " (" +
	        	_ID 				+ INT_TYPE 	+ " PRIMARY KEY," +
	        	_COUNT 				+ INT_TYPE 	+ COMMA_SEP +
	        	
		        COLUMN_NO_SJ 		+ TEXT_TYPE + COMMA_SEP +
		        
		        COLUMN_PHOTO_PATH 	+ TEXT_TYPE + COMMA_SEP +
		        COLUMN_LATITUDE		+ TEXT_TYPE + COMMA_SEP +
		        COLUMN_LONGITUDE	+ TEXT_TYPE + COMMA_SEP +
		        COLUMN_TIMESTAMP   	+ TEXT_TYPE + DEF_NOW + COMMA_SEP +
		        
		        "FOREIGN KEY (" + COLUMN_NO_SJ + ") REFERENCES " + 
	        	DeliveryItemSQL.TABLE + "(" + DeliveryItemSQL.COLUMN_NO_SJ + ") ON DELETE CASCADE " + 
	        " )";
	    
		public static final String SQL_DELETE_ENTRIES =
		    	"DROP TABLE IF EXISTS " + TABLE;
	}
}
