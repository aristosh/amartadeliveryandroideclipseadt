package com.monoceros.amartadelivery.helpers;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;

/*
 * 
 * Resize image and post
 * 
 */

public abstract class BitmapWorker extends AsyncTask<Integer, Void, Bitmap> {
	private Uri mUri;
    
    public BitmapWorker(Uri uri) {
    	this.mUri = uri;
    }

    // BitmapWorkerTask.execute(...)
    // Decode image in background.
    @Override
    protected Bitmap doInBackground(Integer... params) {
        int width = params[0];
        int height = params[1];
        
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mUri.getPath(), options);
        
        options.inSampleSize = calculateSampleSize(options, width, height);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        
        return BitmapFactory.decodeFile(mUri.getPath(), options);
    }
    
    public static int calculateSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
	    // Raw height and width of image
	    final int height = options.outHeight;
	    final int width = options.outWidth;
	    int inSampleSize = 1;
	
	    if (height > reqHeight || width > reqWidth) {
	
	        final int halfHeight = height / 2;
	        final int halfWidth = width / 2;
	
	        // Calculate the largest inSampleSize value that is a power of 2 and keeps both
	        // height and width larger than the requested height and width.
	        while ((halfHeight / inSampleSize) > reqHeight
	                && (halfWidth / inSampleSize) > reqWidth) {
	            inSampleSize *= 2;
	        }
	    }
	
	    return inSampleSize;
	}

    @Override
    protected abstract void onPostExecute(Bitmap bitmap);
    
//  Example onPostExecute implementation
//    protected void onPostExecute(Bitmap bitmap) {
//    	Volley.newRequestQueue(mContext).add(new MultipartRequest(
//				this.mUrl,
//				new Response.Listener<String>() {
//					@Override
//					public void onResponse(String response) {
//						Toast.makeText(mContext, "Success", Toast.LENGTH_SHORT).show();
//					}  // public void onResponse
//				},
//				new Response.ErrorListener() {
//					@Override
//					public void onErrorResponse(VolleyError error) {
//						Toast.makeText(mContext, "error : " + error.getMessage(), Toast.LENGTH_LONG).show();
//					}
//				}, // new Response.ErrorListener
//				this.mFile)
//    	);
//    }
}