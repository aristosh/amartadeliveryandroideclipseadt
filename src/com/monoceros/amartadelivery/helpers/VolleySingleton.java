package com.monoceros.amartadelivery.helpers;

import android.content.Context;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.RetryPolicy;
import com.android.volley.toolbox.Volley;

public class VolleySingleton {
	/** Default Timeout */
	protected final static int REQUEST_TIMEOUT_MS = 0;
	
	/** Default Retry */
	protected final static int REQUEST_RETRY = 0;
	
	/** Default Backoff Multiplier */
	protected final static float BACKOFF_MULT = DefaultRetryPolicy.DEFAULT_BACKOFF_MULT;
	
	private static RequestQueue mRequestQueue;
	
	public static RequestQueue getInstance(Context context) {
		if(mRequestQueue == null)
			mRequestQueue = Volley.newRequestQueue(context);
		
		return mRequestQueue;
	}
	
	public static RetryPolicy getDefaultRetryPolicy() {
		return new DefaultRetryPolicy( REQUEST_TIMEOUT_MS, REQUEST_RETRY, BACKOFF_MULT );
	}
}
