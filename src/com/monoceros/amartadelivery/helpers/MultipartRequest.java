package com.monoceros.amartadelivery.helpers;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyLog;

public class MultipartRequest extends Request<JSONObject> {
	private MultipartEntityBuilder mBuilder;
	private HttpEntity mHttpEntity;
	
	private static final String FILE_PART_NAME = "file";
	
	private Listener mListener;
	private final File mFilePart;
	
	public MultipartRequest(String url, Listener listener, File file) {
	    super(Method.POST, url, listener);
	    mListener = listener;
	    mFilePart = file;
	    mBuilder = MultipartEntityBuilder.create();
	    buildMultipartEntity();
	}
	
	private void buildMultipartEntity(){
	    mBuilder.addPart(FILE_PART_NAME, new FileBody(mFilePart));
	}
	
	public void putBody(String name, int bodyContent) {
		put(name, Integer.toString(bodyContent));
	}
	
	public void put(String name, String bodyContent) {
		try {
			mBuilder.addTextBody(name, bodyContent);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public String getBodyContentType() {
	    return mHttpEntity.getContentType().getValue();
	}
	
	@Override
	public byte[] getBody() throws AuthFailureError {
	    ByteArrayOutputStream bos = new ByteArrayOutputStream() {
	    	@Override
	    	public synchronized void write(byte[] buffer, int offset, int len) {
	    		super.write(buffer, offset, len);
	    	}
	    };
	    
	    try {
	    	mHttpEntity = mBuilder.build();
	    	mHttpEntity.writeTo(bos);
	    } catch (IOException e) {
	    	VolleyLog.e(e.getMessage());
	    }
	    return bos.toByteArray();
	}
	
	@Override
	protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
	    try {
			return Response.success(new JSONObject(new String(response.data)), getCacheEntry());
		} catch (JSONException e) {
			return null;
		}
	}
	
	@Override
	protected void deliverResponse(JSONObject response) {
	    mListener.onResponse(response);
	}
	
	public static interface Listener
			extends Response.Listener<JSONObject>, Response.ErrorListener  {
		public Context getContext();
	}
}
