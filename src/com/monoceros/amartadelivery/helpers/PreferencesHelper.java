package com.monoceros.amartadelivery.helpers;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferencesHelper {	
	public final static String PREF_NAME_CONNECTION_INFO = "PreferencesHelper.CONNECTION_INFO";
	public final static String PREF_KEY_INCOMING_SERVER = "PreferencesHelper.API_INCOMING_SERVER";
	public final static String PREF_KEY_OUTGOING_SERVER = "PreferencesHelper.API_OUTGOING_SERVER";
	
	public static void saveIncomingServer(Context context, String ipAddress) {
		SharedPreferences connectionInfo = context.getSharedPreferences(PREF_NAME_CONNECTION_INFO, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = connectionInfo.edit();
		editor.putString(PREF_KEY_INCOMING_SERVER, ipAddress);
		editor.commit();
	}
	
	public static void saveOutgoingServer(Context context, String ipAddress) {
		SharedPreferences connectionInfo = context.getSharedPreferences(PREF_NAME_CONNECTION_INFO, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = connectionInfo.edit();
		editor.putString(PREF_KEY_OUTGOING_SERVER, ipAddress);
		editor.commit();
	}
	
	public static String getIncomingServer(Context context) {
		SharedPreferences connectionInfo = context.getSharedPreferences(PREF_NAME_CONNECTION_INFO, Context.MODE_PRIVATE);
		return connectionInfo.getString(PREF_KEY_INCOMING_SERVER, "");
	}
	
	public static String getOutgoingServer(Context context) {
		SharedPreferences connectionInfo = context.getSharedPreferences(PREF_NAME_CONNECTION_INFO, Context.MODE_PRIVATE);
		return connectionInfo.getString(PREF_KEY_OUTGOING_SERVER, "");
	}
}
