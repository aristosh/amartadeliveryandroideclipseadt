package com.monoceros.amartadelivery.adapters;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.monoceros.amartadelivery.DeliveryItemDetailActivity;
import com.monoceros.amartadelivery.DeliveryItemDetailActivity.ListItemDetail;
import com.monoceros.amartadelivery.R;

public class DeliveryItemDetailAdapter extends ArrayAdapter<DeliveryItemDetailActivity.ListItemDetail> {
	
	public DeliveryItemDetailAdapter(Context context, int resource,
			List<ListItemDetail> list) {
		super(context, resource, list);
		mResourceId = resource;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if(convertView == null)
			convertView = ((Activity)getContext()).getLayoutInflater().inflate(mResourceId, null);
		
		final DeliveryItemDetailActivity.ListItemDetail itemDetail = getItem(position);
		TextView textAttribute = (TextView) convertView.findViewById(R.id.textAttribute);
		TextView textValue = (TextView) convertView.findViewById(R.id.textValue);
		
		textAttribute.setText(itemDetail.attribute);
		textValue.setText(itemDetail.value);
		
		if(itemDetail.attributeId == R.string.konsumen_telp && !itemDetail.value.equals("")) {
			textValue.setTextColor(getContext().getResources().getColor(android.R.color.holo_green_dark));
			textValue.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent intent = new Intent(Intent.ACTION_DIAL);
					intent.setData(Uri.parse("tel:" + itemDetail.value));
					getContext().startActivity(intent);
				}
			});
		}
		
		return convertView;
	}
	
	private int mResourceId;

}
