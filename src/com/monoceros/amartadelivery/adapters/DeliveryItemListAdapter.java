package com.monoceros.amartadelivery.adapters;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.monoceros.amartadelivery.DeliveryItemDetailActivity;
import com.monoceros.amartadelivery.R;
import com.monoceros.amartadelivery.models.DeliveryItem;

public class DeliveryItemListAdapter extends ArrayAdapter<DeliveryItem> {

	public DeliveryItemListAdapter(Context context, int resource,
			List<DeliveryItem> list) {
		super(context, resource, list);
		mResource = resource;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if(convertView == null) {
			convertView = ((Activity)getContext()).getLayoutInflater().inflate(mResource, null);
		}
		
		final DeliveryItem item = getItem(position);
		
		TextView textNoSj = (TextView) convertView.findViewById(R.id.textNoSj);
		TextView textKonsumenName = (TextView) convertView.findViewById(R.id.textKonsumenName);
		TextView textKonsumenAdrs = (TextView) convertView.findViewById(R.id.textKonsumenAdrs);
		ImageView imageStatusCompleted = (ImageView) convertView.findViewById(R.id.imageStatusCompleted);
		
		textNoSj.setText(item.getNoSj());
		textKonsumenName.setText(item.getKonsumenName());
		textKonsumenAdrs.setText(item.getKonsumenAdrs());
		
		imageStatusCompleted.setVisibility((item.checkAutograph() && item.checkLocation() &&
				item.checkPhoto()) ? View.VISIBLE : View.INVISIBLE);
		
		convertView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getContext(), DeliveryItemDetailActivity.class);
				intent.putExtra(DeliveryItem.EXTRA_KEY, item);
				((Activity)getContext()).startActivityForResult(intent, 0);
			}
		});
		
		return convertView;
	}

	private int mResource;
}
