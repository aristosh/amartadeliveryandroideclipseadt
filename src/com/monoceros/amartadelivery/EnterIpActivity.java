package com.monoceros.amartadelivery;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.monoceros.amartadelivery.helpers.PreferencesHelper;

public class EnterIpActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_enter_ip);
		
		getActionBar().setHomeButtonEnabled(true);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		if (savedInstanceState == null) {
			getFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		} else if(id == android.R.id.home) {
			onBackPressed();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment implements OnClickListener {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_enter_ip,
					container, false);
			
			mEditIncomingIpAddress = (EditText) rootView.findViewById(R.id.editIncomingIpAddress);
			mEditOutgoingIpAddress = (EditText) rootView.findViewById(R.id.editOutgoingIpAddress);
			mButtonOk = (Button) rootView.findViewById(R.id.buttonOk);
			
			String incomingServer = PreferencesHelper.getIncomingServer(getActivity());
			mEditIncomingIpAddress.setText(incomingServer.equals("") ? getString(R.string.api_incoming_server) : incomingServer);
			
			String outgoingServer = PreferencesHelper.getOutgoingServer(getActivity());
			mEditOutgoingIpAddress.setText(outgoingServer.equals("") ? getString(R.string.api_outgoing_server) : outgoingServer);
			
			mButtonOk.setOnClickListener(this);
			return rootView;
		}
		
		@Override
		public void onClick(View v) {
			String incomingIpAddress = mEditIncomingIpAddress.getText().toString();
			PreferencesHelper.saveIncomingServer(getActivity(), incomingIpAddress);
			
			String outgoingIpAddress = mEditOutgoingIpAddress.getText().toString();
			PreferencesHelper.saveOutgoingServer(getActivity(), outgoingIpAddress);
			
			getActivity().finish();	
		}
		
		private EditText mEditIncomingIpAddress;
		private EditText mEditOutgoingIpAddress;
		private Button mButtonOk;
	}
}
