package com.monoceros.amartadelivery;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.location.LocationClient;
import com.google.android.gms.maps.model.LatLng;
import com.monoceros.amartadelivery.helpers.SQLHelper;
import com.monoceros.amartadelivery.models.DeliveryItem;
import com.monoceros.customviews.CanvasView;
import com.monoceros.locationactivity.LocationActivity;

public class AutographCanvasActivity extends LocationActivity {
	public final static String EXTRA_LOCATION = "AutographCanvasActivity.Location";
	
	public static LocationClient locationClient;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_autograph_canvas);
	
		locationClient = getLocationClient();
		
		if (savedInstanceState == null) {
			PlaceholderFragment fragment = new PlaceholderFragment();
			
			Bundle args = new Bundle();
			args.putParcelable(DeliveryItem.EXTRA_KEY, getIntent().getParcelableExtra(DeliveryItem.EXTRA_KEY));
			fragment.setArguments(args);
			
			getFragmentManager().beginTransaction()
					.add(R.id.container, fragment).commit();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.autograph_canvas, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment implements OnClickListener {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(
					R.layout.fragment_autograph_canvas, container, false);
			
			mItem = getArguments().getParcelable(DeliveryItem.EXTRA_KEY);
			SQLHelper sqlHelper = new SQLHelper(getActivity());
			mItem = DeliveryItem.selecLocal(mItem.getNoSj(), sqlHelper);
			sqlHelper.close();
			
			mCanvasAutograph = (CanvasView) rootView.findViewById(R.id.canvasAutograph);
			mButtonOk = (ImageView) rootView.findViewById(R.id.buttonOk);
			mButtonClear = (ImageView) rootView.findViewById(R.id.buttonClear);
			
			if(!mItem.getAutographPath().equals("")) {
				Bitmap bm = BitmapFactory.decodeFile(mItem.getAutographPath());
				if(bm != null)
					mCanvasAutograph.loadBitmap(bm);
			}
			
			mButtonOk.setOnClickListener(this);
			mButtonClear.setOnClickListener(this);
			
			return rootView;
		}
		
		@Override
		public void onViewCreated(View view, Bundle savedInstanceState) {
			super.onViewCreated(view, savedInstanceState);
		}
		@Override
		public void onClick(View v) {
			int id = v.getId();
			if(id == R.id.buttonOk) {
				try {
					saveAsPicture();
					updateDatabase();
				} catch (IOException e) {
					Toast.makeText(getActivity(), getResources().getString(R.string.error_saving_autograph), Toast.LENGTH_SHORT).show();
				}
				getActivity().finish();
			} else if(id == R.id.buttonClear) {
				mCanvasAutograph.cleanup();
			}
		}
		
		private void saveAsPicture() throws IOException {
			mCanvasAutograph.setDrawingCacheEnabled(true);
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			mCanvasAutograph.getDrawingCache().compress(Bitmap.CompressFormat.PNG, 90, bytes);
			mCanvasAutograph.destroyDrawingCache();
			
			File f = createImageFile();
			f.createNewFile();
			FileOutputStream fo = new FileOutputStream(f);
			fo.write(bytes.toByteArray());
			fo.close();
			
			if(!mItem.getAutographPath().equals("")) {
				File oldF = new File(mItem.getAutographPath());
				oldF.delete();
			}
			mItem.setAutographPath(f.getAbsolutePath());
			
			Location lastLocation = locationClient.getLastLocation();
			mItem.setLocation(new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude()));
			
			mItem.setAutographTimestamp(new Date());
			
			Intent mediaScanIntent = new Intent( Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
			mediaScanIntent.setData(Uri.fromFile(f));
			getActivity().sendBroadcast(mediaScanIntent);
		}
		
		private File createImageFile() throws IOException {
		    // Create an image file name	
		    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
		    String imageFileName = "IMG_" + timeStamp + ".jpeg";
		    File storageDir = Environment.getExternalStoragePublicDirectory(
		    		Environment.DIRECTORY_DCIM);
		    
		    File image = new File(storageDir.getPath() + File.separator + imageFileName);

		    return image;
		}
		
		private void updateDatabase() {
			SQLHelper sqlHelper = new SQLHelper(getActivity());
			mItem.updateLocal(sqlHelper);
			sqlHelper.close();
		}
		
		private DeliveryItem mItem;
		private CanvasView mCanvasAutograph;
		private ImageView mButtonOk;
		private ImageView mButtonClear;
	}
	
	@Override
	public void onBackPressed() {
		new AlertDialog.Builder(this)
		.setTitle(getString(R.string.confirmation))
		.setMessage(getString(R.string.cancel_signing))
		.setIcon(android.R.drawable.ic_dialog_alert)
		.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
		    @Override
			public void onClick(DialogInterface dialog, int whichButton) {
		    	AutographCanvasActivity.super.onBackPressed();
		    }
		})
		.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) { }
		})
		.setOnCancelListener(new OnCancelListener() {
			@Override
			public void onCancel(DialogInterface dialog) { }
		})
		.show();
	}
}
