package com.monoceros.amartadelivery;

import java.util.List;

import com.monoceros.amartadelivery.helpers.SQLHelper;
import com.monoceros.amartadelivery.models.DeliveryItem;
import com.monoceros.amartadelivery.models.DeliveryOrder;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		if (savedInstanceState == null) {
			getFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			
			return rootView;
		}
		
		@Override
		public void onViewCreated(View view, Bundle savedInstanceState) {
			super.onViewCreated(view, savedInstanceState);
			
			if( !isLocationServiceEnabled() ) {
				Toast.makeText(getActivity(), getString(R.string.error_location_service), Toast.LENGTH_LONG).show();
	        	Intent gpsOptionsIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);  
	        	startActivity(gpsOptionsIntent);
	        	getActivity().finish();
	        	return;
			}
			
			SQLHelper sqlHelper = new SQLHelper(getActivity());
			DeliveryOrder order = DeliveryOrder.selectLocal(sqlHelper);
			if(order != null) {				
				Intent intent = new Intent(getActivity(), DeliveryItemListActivity.class);
				intent.putExtra(DeliveryOrder.EXTRA_NAME, order);
				startActivity(intent);
			} else {
				Intent intent = new Intent(getActivity(), EnterOrderActivity.class);
				startActivity(intent);
			}
			getActivity().finish();
			sqlHelper.close();
		}
		
		private boolean isLocationServiceEnabled() {
			LocationManager locManager = (LocationManager) getActivity().getSystemService(Activity.LOCATION_SERVICE);  

	        if (locManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){  
	           //GPS enabled
	           return true;
	        }
	           
	        else{
	           //GPS disabled
	           return false;
	        }
		}
	}
}
