package com.monoceros.amartadelivery;

import java.util.List;

import android.app.ActionBar;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.example.touch.TouchImageView;
import com.monoceros.amartadelivery.helpers.BitmapLruCache;
import com.monoceros.amartadelivery.helpers.BitmapWorker;
import com.monoceros.amartadelivery.helpers.SQLHelper;
import com.monoceros.amartadelivery.models.Photo;
import com.monoceros.swipeview.SwipeViewActivity;

public class FullscreenPhotoActivity extends SwipeViewActivity {
	public final static String EXTRA_NO_SJ = "FullscreenPhotoActivity.noSj";
	public final static String EXTRA_IMAGE_PATH = "FullscreenPhotoActivity.imagePath";
	
	private List<Photo> mPhotos;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		View decorView = getWindow().getDecorView();
		// Hide the status bar.
		int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
		decorView.setSystemUiVisibility(uiOptions);
		// Remember that you should never show the action bar if the
		// status bar is hidden, so hide that too if necessary.
		ActionBar actionBar = getActionBar();
		actionBar.hide();
		
		String imagePath = getIntent().getStringExtra(EXTRA_IMAGE_PATH);
		int index = getActivePhotoIndex(imagePath, mPhotos);
		getViewPager().setCurrentItem(index);

	}
	
	private int getActivePhotoIndex(String imagePath, List<Photo> photos) {
		int i = 0;
		
		for(int size = photos.size();i < size; i++)
			if(photos.get(i).getPhotoPath().equals(imagePath))
				return i;
		
		return -1;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.fullscreen_photo, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public ViewPager getViewPager() {
		return (ViewPager) findViewById(R.id.pager);
	}

	@Override
	public PagerAdapter getPagerAdapter() {
		String noSj = getIntent().getStringExtra(EXTRA_NO_SJ);
		SQLHelper sqlHelper = new SQLHelper(FullscreenPhotoActivity.this);
		mPhotos = Photo.selectLocal(noSj, sqlHelper);
		sqlHelper.close();

		return new FragmentPagerAdapter(getSupportFragmentManager()) {
			@Override
			public int getCount() {
				return mPhotos.size();
			}
			
			@Override
			public Fragment getItem(int position) {
				return new PlaceholderFragment(Uri.parse(mPhotos.get(position).getPhotoPath()));
			}
		};
	}

	@Override
	public int getLayoutResId() {
		return R.layout.activity_fullscreen_photo;
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {
		private Uri mUri;
		private TouchImageView mImageFullscreenPhoto;
		private BitmapLruCache mCache;
		
		public PlaceholderFragment(Uri uri) {
			this.mUri = uri;
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_fullscreen_photo, null);
			
			mCache = BitmapLruCache.getInstance();
			mImageFullscreenPhoto = (TouchImageView) rootView.findViewById(R.id.imageFullscreenPhoto); 
			
			DisplayMetrics dm = new DisplayMetrics();
			getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
			
			if(mCache.getBitmap(mUri.getPath()) == null) {
				BitmapWorker bw = new BitmapWorker(mUri) {
					@Override
					protected void onPostExecute(Bitmap bitmap) {
						mImageFullscreenPhoto.setImageBitmap(bitmap);
						mCache.putBitmap(mUri.getPath(), bitmap);
					}
				};
				
				bw.execute(dm.widthPixels, dm.heightPixels);
			} else {
				mImageFullscreenPhoto.setImageBitmap(mCache.getBitmap(mUri.getPath()));
			}
			return rootView;
		}
		
		@Override
		public void onViewCreated(View view, Bundle savedInstanceState) {
			super.onViewCreated(view, savedInstanceState);
		}
	}
}
