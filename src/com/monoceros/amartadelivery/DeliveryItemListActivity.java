package com.monoceros.amartadelivery;

import android.app.Activity;
import android.app.ListFragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.monoceros.amartadelivery.adapters.DeliveryItemListAdapter;
import com.monoceros.amartadelivery.helpers.SQLHelper;
import com.monoceros.amartadelivery.models.DeliveryItem;
import com.monoceros.amartadelivery.models.DeliveryOrder;
import com.monoceros.amartadelivery.models.Photo;
import com.monoceros.locationactivity.LocationActivity;

public class DeliveryItemListActivity extends LocationActivity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_delivery_item_list);
		
		DeliveryOrder order = getIntent().getParcelableExtra(DeliveryOrder.EXTRA_NAME);
		if (savedInstanceState == null) {
			mFragment = new PlaceholderFragment();
			Bundle args = new Bundle();
			args.putParcelable(DeliveryOrder.EXTRA_NAME, order);
			mFragment.setArguments(args);
			getFragmentManager().beginTransaction()
					.add(R.id.container, mFragment).commit();
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.delivery_item_list, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return mFragment.onOptionsItemSelected(item);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		mFragment.onActivityResult(requestCode, resultCode, data);
	}
	
	private PlaceholderFragment mFragment;

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends ListFragment {
		
		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(
					R.layout.fragment_delivery_item_list, container, false);
	
			return rootView;
		}
		
		@Override
		public void onViewCreated(View view, Bundle savedInstanceState) {
			super.onViewCreated(view, savedInstanceState);
			
			mActivity = getActivity();
			mOrder = getArguments().getParcelable(DeliveryOrder.EXTRA_NAME);
			setListAdapter(new DeliveryItemListAdapter(mActivity,
					R.layout.cell_delivery_item, mOrder.getItems()));
			
			View headerView = mActivity.getLayoutInflater().inflate(R.layout.cell_delivery_order_detail, null);
			((TextView) headerView.findViewById(R.id.textValDealerName)).setText(mOrder.getDealerName());
			((TextView) headerView.findViewById(R.id.textValNoDo)).setText(mOrder.getNoDo());
			((TextView) headerView.findViewById(R.id.textValDoDate)).setText(mOrder.getDoDate());
			((TextView) headerView.findViewById(R.id.textValNoP)).setText(mOrder.getNoP());
			((TextView) headerView.findViewById(R.id.textValDriverName)).setText(mOrder.getDriverName());
			((TextView) headerView.findViewById(R.id.textValShift)).setText(mOrder.getShift());
			getListView().addHeaderView(headerView);
		}
		
		@Override
		public boolean onOptionsItemSelected(MenuItem item) {
			int id = item.getItemId();
			if(id == R.id.action_other_doc) {
				Intent intent = new Intent(mActivity, EnterOrderActivity.class);
				intent.putExtra(DeliveryOrder.EXTRA_NAME, mOrder);
				startActivity(intent);
			} else if(id == R.id.action_upload) {
				syncPhotosData();
				
				//if(verifyData())
					postData();
				
				return true;
			}
			return super.onOptionsItemSelected(item);
		}
		
		@Override
		public void onActivityResult(int requestCode, int resultCode,
				Intent data) {
			SQLHelper sqlHelper = new SQLHelper(mActivity);
			for(DeliveryItem item : mOrder.getItems()) {
				item.selectLocal(item.getNoSj(), sqlHelper);
			}
			sqlHelper.close();
			((DeliveryItemListAdapter)getListAdapter()).notifyDataSetChanged();
		}
		
		private void syncPhotosData() {
			SQLHelper sqlHelper = new SQLHelper(mActivity);
			for (final DeliveryItem item : mOrder.getItems()) {
				item.selectLocal(item.getNoSj(), sqlHelper);
				item.setPhotos(Photo.selectLocal(item.getNoSj(), sqlHelper));
			}
			sqlHelper.close();
		}
		
		private boolean verifyData() {
			for(DeliveryItem item : mOrder.getItems()) {
				if(!item.checkAutograph()) {
					Toast.makeText(mActivity, getString(R.string.error_autograph, item.getNoSj()), Toast.LENGTH_SHORT).show();
					return false;
				}
				
				if(!item.checkLocation()) {
					Toast.makeText(mActivity, getString(R.string.error_location, item.getNoSj()), Toast.LENGTH_SHORT).show();
					return false;
				}
				
				if(!item.checkPhoto()) {
					Toast.makeText(mActivity, getString(R.string.error_photo, item.getNoSj()), Toast.LENGTH_SHORT).show();
					return false;
				}
			}
			
			return true;
		}
		
		private void postData() {
			final SQLHelper sqlHelper = new SQLHelper(mActivity);
			mOrder.postRemote(new DeliveryOrder.PostRemoteCallback(mActivity) {
				@Override
				public void onErrorResponse(VolleyError error) {
					Toast.makeText(getContext(), getString(R.string.error_volley), Toast.LENGTH_SHORT).show();
				}
				
				@Override
				public void onSuccess(int id) {
					for (final DeliveryItem item : mOrder.getItems()) {
						if(!item.checkAutograph()) {
							Toast.makeText(mActivity, getString(R.string.error_autograph, item.getNoSj()), Toast.LENGTH_SHORT).show();
							continue;
						}
						
						if(!item.checkLocation()) {
							Toast.makeText(mActivity, getString(R.string.error_location, item.getNoSj()), Toast.LENGTH_SHORT).show();
							continue;
						}
						
						if(!item.checkPhoto()) {
							Toast.makeText(mActivity, getString(R.string.error_photo, item.getNoSj()), Toast.LENGTH_SHORT).show();
							continue;
						}
						
						item.postRemote(id, new DeliveryItem.PostCallback(mActivity) {
							@Override
							public void onErrorResponse(VolleyError error) {
								Toast.makeText(getContext(), getString(R.string.error_volley), Toast.LENGTH_SHORT).show();
							}
							
							@Override
							public void onSuccess(int id) {
								synchronized (mLock) {									
									// delete order if all item success
									item.deleteLocal(sqlHelper);
									mOrder.getItems().remove(item);
									((DeliveryItemListAdapter) getListAdapter()).notifyDataSetChanged();
									
									if(mOrder.getItems().size() == 0) {
										mOrder.deleteLocal(sqlHelper);
										Toast.makeText(mActivity, getString(R.string.upload_success), Toast.LENGTH_SHORT).show();
										
										Intent intent = new Intent(mActivity, EnterOrderActivity.class);
										intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
										startActivity(intent);
									}									
									
									for(final Photo photo : item.getPhotos()) {
										photo.postRemote(id, new Photo.PostCallback(mActivity) {
											@Override
											public void onErrorResponse(VolleyError error) {
												Toast.makeText(getContext(), getString(R.string.error_volley), Toast.LENGTH_SHORT).show();
											}

											@Override
											public void onSuccess(int id) {
												Photo.deleteLocal(photo.getPhotoPath(), sqlHelper);
											}

											@Override
											public void onError(String err) {
												Toast.makeText(mActivity, err, Toast.LENGTH_SHORT).show();
											}
										});
									}	
								}
							}

							@Override
							public void onError(String err) {
								Toast.makeText(mActivity, err, Toast.LENGTH_SHORT).show();
							}
						});
					}
				}
				
				@Override
				public void onError(String err) {
					Toast.makeText(mActivity, err, Toast.LENGTH_SHORT).show();
				}
			});
		}
		
		private DeliveryOrder mOrder;
		private Object mLock = new Object();
		private Activity mActivity;
	}
}
