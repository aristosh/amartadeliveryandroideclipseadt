package com.monoceros.amartadelivery.models;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.os.Parcelable;

import com.android.volley.RequestQueue;
import com.google.android.gms.maps.model.LatLng;
import com.monoceros.amartadelivery.helpers.MultipartRequest;
import com.monoceros.amartadelivery.helpers.PreferencesHelper;
import com.monoceros.amartadelivery.helpers.SQLHelper;
import com.monoceros.amartadelivery.helpers.SQLSchemaHelper;
import com.monoceros.amartadelivery.helpers.VolleySingleton;

@SuppressLint("SimpleDateFormat")
public class Photo implements Parcelable {
	public final static String PROTO_DELIVERY_ITEM_ID = "_delivery_item_id";
	public final static String PROTO_LAT = "lat";
	public final static String PROTO_LNG = "lng";
	public final static String PROTO_TIMESTAMP = "photo_timestamp";
	
	private final static String DATE_FORMAT = "yyyy-MM-dd HH:mm";
	
	public Photo(String photoPath, LatLng location, Date timestamp) {
		this.photoPath = photoPath;
		this.location = location;
		this.timestamp = timestamp;
	}
	
	public String getPhotoPath() {
		return photoPath;
	}
	
	public LatLng getLocation() {
		return this.location;
	}
	
	public Date getTimestamp() {
		return this.timestamp;
	}

	public static List<Photo> selectLocal(String noSj, SQLHelper sqlHelper) {
		if(sqlHelper == null)
			return null;
		
		List<Photo> photos = new ArrayList<Photo>();
		SQLiteDatabase db = sqlHelper.getReadableDatabase();
		String[] columns = {
			SQLSchemaHelper.PhotoSQL.COLUMN_PHOTO_PATH,
			SQLSchemaHelper.PhotoSQL.COLUMN_LATITUDE,
			SQLSchemaHelper.PhotoSQL.COLUMN_LONGITUDE,
			SQLSchemaHelper.PhotoSQL.COLUMN_TIMESTAMP
		};
	
		String[] selectionArgs = {noSj};
		Cursor c = db.query(SQLSchemaHelper.PhotoSQL.TABLE, columns,
				SQLSchemaHelper.PhotoSQL.COLUMN_NO_SJ + "=?", selectionArgs,
				null, null, null);
		
		SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT);
		while(c.moveToNext()) {
			Date timestamp;
			try {
				timestamp = df.parse(c.getString(3));
			} catch (ParseException e) {
				timestamp = null;
			}
			
			photos.add(new Photo(c.getString(0),
					new LatLng(c.getDouble(1), c.getDouble(2)),
					timestamp));
		}
		db.close();
		return photos;
	}
	
	public void postRemote(int itemId, PostCallback callback) {
		Context context = callback.getContext();
		RequestQueue requestQueue = VolleySingleton.getInstance(context);
		String url = PreferencesHelper.getOutgoingServer(context) + "/photo";;
		SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT);
		
		MultipartRequest request = new MultipartRequest(url, callback, new File(this.photoPath));
		request.put(PROTO_DELIVERY_ITEM_ID, String.valueOf(itemId));
		request.put(PROTO_LAT, String.valueOf(this.getLocation().latitude));
		request.put(PROTO_LNG, String.valueOf(this.getLocation().longitude));
		request.put(PROTO_TIMESTAMP, df.format(this.timestamp));
		
		request.setRetryPolicy(VolleySingleton.getDefaultRetryPolicy());
		requestQueue.add(request);
	}
	
	public void insertLocal(String noSj, SQLHelper sqlHelper) {
		SQLiteDatabase db = sqlHelper.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(SQLSchemaHelper.PhotoSQL.COLUMN_NO_SJ, noSj);
		values.put(SQLSchemaHelper.PhotoSQL.COLUMN_PHOTO_PATH, this.photoPath);
		values.put(SQLSchemaHelper.PhotoSQL.COLUMN_LATITUDE, this.location.latitude);
		values.put(SQLSchemaHelper.PhotoSQL.COLUMN_LONGITUDE, this.location.longitude);
		db.insert(SQLSchemaHelper.PhotoSQL.TABLE, null, values);
		db.close();
	}
	
	public static boolean deleteLocal(String path, SQLHelper sqlHelper) {
		SQLiteDatabase db = sqlHelper.getWritableDatabase();
		String[] whereArgs = {
			path
		};
		int rows = db.delete(SQLSchemaHelper.PhotoSQL.TABLE, SQLSchemaHelper.PhotoSQL.COLUMN_PHOTO_PATH + "=?", whereArgs);
		db.close();
		
		return rows > 0;
	}
	
	public static void deleteAllLocal(SQLHelper sqlHelper) {
		SQLiteDatabase db = sqlHelper.getWritableDatabase();
		db.delete(SQLSchemaHelper.PhotoSQL.TABLE, null, null);
		db.close();
	}
	
	public void deleteLocal(SQLHelper sqlHelper) {
		SQLiteDatabase db = sqlHelper.getWritableDatabase();
		String[] whereArgs = {this.photoPath};
		db.delete(SQLSchemaHelper.PhotoSQL.TABLE,
				SQLSchemaHelper.PhotoSQL.COLUMN_PHOTO_PATH + "=?", whereArgs);
		db.close();
	}
	
	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel out, int flags) {
		out.writeString(this.photoPath);
		out.writeParcelable(getLocation(), flags);
		out.writeSerializable(timestamp);
	}
	
	private Photo(Parcel in) {
		this.photoPath = in.readString();
		this.location = in.readParcelable(LatLng.class.getClassLoader());
		this.timestamp = (Date) in.readSerializable();
	}
	
	public static final Parcelable.Creator<Photo> CREATOR
			= new Parcelable.Creator<Photo>() {
		
		public Photo createFromParcel(Parcel in) {
		    return new Photo(in);
		}
		
		public Photo[] newArray(int size) {
		    return new Photo[size];
		}
	};
	
	private String photoPath;
	private LatLng location;
	private Date timestamp;
	
	public static abstract class PostCallback implements MultipartRequest.Listener {		
		public PostCallback(Context context) {
			this.mContext = context;
		}
		
		public abstract void onSuccess(int id);
		public abstract void onError(String err);
		
		@Override
		public void onResponse(JSONObject response) {
			String msg = response.optString("msg");
			if(msg.equals("success"))
				onSuccess(response.optInt("id"));
			else if(msg.equals("error"))
				onError(response.optString("err"));
		}

		@Override
		public Context getContext() {
			return mContext;
		}
		
		private Context mContext;
		
	}
}
