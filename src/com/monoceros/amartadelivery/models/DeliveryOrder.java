package com.monoceros.amartadelivery.models;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.os.Parcelable;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.JsonObjectRequest;
import com.monoceros.amartadelivery.helpers.PreferencesHelper;
import com.monoceros.amartadelivery.helpers.SQLHelper;
import com.monoceros.amartadelivery.helpers.SQLSchemaHelper;
import com.monoceros.amartadelivery.helpers.VolleySingleton;

@SuppressLint({ "SimpleDateFormat", "DefaultLocale" })
public class DeliveryOrder implements Parcelable{
	public final static String EXTRA_NAME = "DeliveryOrder";
	
	public final static String PROTO_ID_DEALER = "id_dealer";
	public final static String PROTO_DEALER_NAME = "dealer_name";
	public final static String PROTO_NO_DO = "no_do";
	public final static String PROTO_DO_DATE = "do_date";
	public final static String PROTO_DRIVER_NAME = "driver_name";
	public final static String PROTO_NO_P = "no_p";
	public final static String PROTO_SHIFT = "shift";
	public final static String PROTO_ITEMS = "rows";
	
	public DeliveryOrder(String idDealer, String dealerName, String noDo,
			String doDate, String driverName, String noP, String shift) {
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm a");
		
		this.idDealer = idDealer.trim();
		this.dealerName = dealerName.trim();
		this.noDo = noDo;
		try {
			this.doDate = format.parse(doDate);
		} catch (ParseException e) {
			this.doDate = null;
		}
		this.driverName = driverName.trim();
		this.noP = noP.trim();
		this.shift = shift.trim();
	}
	
	public static void getRemote(String noDo, GetRemoteCallback callback) {
		Context context = callback.getContext();
		RequestQueue requestQueue = VolleySingleton.getInstance(context);
		String url = PreferencesHelper.getIncomingServer(context) + "/do?no=" + noDo.toUpperCase();
		
		JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, callback, callback);
		requestQueue.add(request);
	}
	
	public void postRemote(PostRemoteCallback callback) {
		Context context = callback.getContext();
		RequestQueue requestQueue = VolleySingleton.getInstance(context);
		String url = PreferencesHelper.getOutgoingServer(context) + "/delivery_order";
		
		JSONObject body = new JSONObject();
		try {
			body.put(PROTO_ID_DEALER, this.getIdDealer());
			body.put(PROTO_DEALER_NAME, this.getDealerName());
			body.put(PROTO_NO_DO, this.getNoDo());
			body.put(PROTO_DO_DATE, this.getDoDate());
			body.put(PROTO_DRIVER_NAME, this.getDriverName());
			body.put(PROTO_NO_P, this.getNoP());
			body.put(PROTO_SHIFT, this.getShift());
		} catch (JSONException e) {
			e.printStackTrace();
			return;
		}
		
		JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, body, callback, callback);
		request.setRetryPolicy(VolleySingleton.getDefaultRetryPolicy());
		requestQueue.add(request);
	}
	
	public void deleteRemote(DeleteRemoteCallback callback) {
		Context context = callback.getContext();
		RequestQueue requestQueue = VolleySingleton.getInstance(context);
		String url = PreferencesHelper.getOutgoingServer(context) + "/delivery_order?no_do=" + this.noDo;
		
		JsonObjectRequest request = new JsonObjectRequest(Request.Method.DELETE, url, null, callback, callback);
		request.setRetryPolicy(VolleySingleton.getDefaultRetryPolicy());
		requestQueue.add(request);
	}
	
	/*
	 * Source : SQLite
	 */
	public static DeliveryOrder selectLocal(SQLHelper sqlHelper) {
		if(sqlHelper == null)
			return null;
		
		SQLiteDatabase db = sqlHelper.getReadableDatabase();
		String[] columns = {
			SQLSchemaHelper.DeliveryOrderSQL.COLUMN_ID_DEALER,
			SQLSchemaHelper.DeliveryOrderSQL.COLUMN_DEALER_NAME,
			SQLSchemaHelper.DeliveryOrderSQL.COLUMN_NO_DO,
			SQLSchemaHelper.DeliveryOrderSQL.COLUMN_DO_DATE,
			SQLSchemaHelper.DeliveryOrderSQL.COLUMN_DRIVER_NAME,
			SQLSchemaHelper.DeliveryOrderSQL.COLUMN_NO_P,
			SQLSchemaHelper.DeliveryOrderSQL.COLUMN_SHIFT
		};
		Cursor c = db.query(SQLSchemaHelper.DeliveryOrderSQL.TABLE, columns, null, null, null, null, null);
		DeliveryOrder order = null;
		if(c.moveToFirst()) {
			order = new DeliveryOrder(c.getString(0), c.getString(1), c.getString(2), c.getString(3),
					c.getString(4), c.getString(5), c.getString(6));
			
			order.setItems(DeliveryItem.selectListLocal(order.getNoDo(), sqlHelper));
		}
		db.close();
		
		return order;
	}
	
	public void insertLocal(SQLHelper sqlHelper) {
		SQLiteDatabase db = sqlHelper.getWritableDatabase();
		ContentValues values = new ContentValues();
		
		values.put(SQLSchemaHelper.DeliveryOrderSQL.COLUMN_ID_DEALER, this.getIdDealer());
		values.put(SQLSchemaHelper.DeliveryOrderSQL.COLUMN_DEALER_NAME, this.getDealerName());
		values.put(SQLSchemaHelper.DeliveryOrderSQL.COLUMN_NO_DO, this.getNoDo());
		values.put(SQLSchemaHelper.DeliveryOrderSQL.COLUMN_DO_DATE, this.getDoDate());
		values.put(SQLSchemaHelper.DeliveryOrderSQL.COLUMN_DRIVER_NAME, this.getDriverName());
		values.put(SQLSchemaHelper.DeliveryOrderSQL.COLUMN_NO_P, this.getNoP());
		values.put(SQLSchemaHelper.DeliveryOrderSQL.COLUMN_SHIFT, this.getShift());
		
		long id = db.insert(SQLSchemaHelper.DeliveryOrderSQL.TABLE, null, values);
		db.close();
		
		if(id > -1) {
			for(int i = 0, size = this.items.size(); i < size; i++) {
				items.get(i).insertLocal(noDo, sqlHelper);
			}
		}
	}

	
	public boolean deleteLocal(SQLHelper sqlHelper) {
		SQLiteDatabase db = sqlHelper.getWritableDatabase();
		String[] whereArgs = {
				this.noDo
		};
		int rows = db.delete(SQLSchemaHelper.DeliveryOrderSQL.TABLE, SQLSchemaHelper.DeliveryOrderSQL.COLUMN_NO_DO + "=?", whereArgs);
		db.close();
		
		return rows > 0;
	}
	
	public static void deleteAllLocal(SQLHelper sqlHelper) {
		SQLiteDatabase db = sqlHelper.getWritableDatabase();
		db.delete(SQLSchemaHelper.DeliveryOrderSQL.TABLE, null, null);
		db.close();
	}
	
	public void setItems(List<DeliveryItem> items) {
		this.items = items;
	}
	
	public String getIdDealer() {
		return idDealer;
	}
	public String getDealerName() {
		return dealerName;
	}
	public String getNoDo() {
		return noDo;
	}
	public String getDoDate() {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm a");
		return format.format(doDate);
	}
	public String getDriverName() {
		return driverName;
	}
	public String getNoP() {
		return noP;
	}
	public String getShift() {
		return shift;
	}
	public List<DeliveryItem> getItems() {
		return items;
	}
	
	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel out, int flags) {
		out.writeString(idDealer);
		out.writeString(dealerName);
		out.writeString(noDo);
		out.writeSerializable(doDate);
		out.writeString(driverName);
		out.writeString(noP);
		out.writeString(shift);
		out.writeList(items);
	}

	public static final Parcelable.Creator<DeliveryOrder> CREATOR
			= new Parcelable.Creator<DeliveryOrder>() {
		
		public DeliveryOrder createFromParcel(Parcel in) {
		    return new DeliveryOrder(in);
		}
		
		public DeliveryOrder[] newArray(int size) {
		    return new DeliveryOrder[size];
		}
	};
	
	private DeliveryOrder(Parcel in) {
		idDealer = in.readString();
		dealerName = in.readString();
		noDo = in.readString();
		doDate = (Date) in.readSerializable();
		driverName = in.readString();
		noP = in.readString();
		shift = in.readString();
		items = new ArrayList<DeliveryItem>(); in.readList(items, DeliveryItem.class.getClassLoader());
	}
	
	public static DeliveryOrder parseJSON(JSONObject response) {
		if(response == null)
			return null;
		
		List<DeliveryItem> items = new ArrayList<DeliveryItem>();
		
		JSONArray itemsJSON = response.optJSONArray(PROTO_ITEMS);
		if(itemsJSON != null) {
			for(int i = 0, size = itemsJSON.length(); i < size; i++) {
				items.add( DeliveryItem.parseJSON( itemsJSON.optJSONObject(i) ) );
			}
		}
		
		DeliveryOrder order = new DeliveryOrder(
				response.optString(PROTO_ID_DEALER),
				response.optString(PROTO_DEALER_NAME),
				response.optString(PROTO_NO_DO),
				response.optString(PROTO_DO_DATE),
				response.optString(PROTO_DRIVER_NAME),
				response.optString(PROTO_NO_P),
				response.optString(PROTO_SHIFT));
		
		order.setItems(items);
		
		return order;
	}

	private String idDealer;
	private String dealerName;
	private String noDo;
	private Date doDate;
	private String driverName;
	private String noP;
	private String shift;
	private List<DeliveryItem> items;
	
	public static abstract class GetRemoteCallback implements Listener<JSONObject>, ErrorListener {		
		public GetRemoteCallback(Context context) {
			this.mContext = context;
		}
		
		@Override
		public void onResponse(JSONObject response) {
			onResponse(DeliveryOrder.parseJSON(response));
		}
		
		public Context getContext() {
			return mContext;
		}
		
		
		public abstract void onResponse(DeliveryOrder result);
		
		
		private Context mContext;
	}

	public static abstract class PostRemoteCallback implements Listener<JSONObject>, ErrorListener {
		public PostRemoteCallback(Context context) {
			mContext = context;
		}
		
		public abstract void onSuccess(int id);
		public abstract void onError(String err);
		
		@Override
		public void onResponse(JSONObject response) {
			String msg = response.optString("msg");
			if(msg.equals("success"))
				onSuccess(response.optInt("id"));
			else if(msg.equals("error"))
				onError(response.optString("err"));
		}
		
		public Context getContext() {
			return mContext;
		}
		
		private Context mContext;
	}
	
	public static abstract class DeleteRemoteCallback implements Listener<JSONObject>, ErrorListener {
		public DeleteRemoteCallback(Context context) {
			mContext = context;
		}
		
		public abstract void onSuccess();
		public abstract void onError(String err);
		
		@Override
		public void onResponse(JSONObject response) {
			String msg = response.optString("msg");
			if(msg.equals("success"))
				onSuccess();
			else if(msg.equals("error"))
				onError(response.optString("err"));
		}
		
		public Context getContext() {
			return mContext;
		}
		
		private Context mContext;
	}
}
