package com.monoceros.amartadelivery.models;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import com.android.volley.RequestQueue;
import com.google.android.gms.maps.model.LatLng;
import com.monoceros.amartadelivery.helpers.MultipartRequest;
import com.monoceros.amartadelivery.helpers.PreferencesHelper;
import com.monoceros.amartadelivery.helpers.SQLHelper;
import com.monoceros.amartadelivery.helpers.SQLSchemaHelper;
import com.monoceros.amartadelivery.helpers.VolleySingleton;

@SuppressLint("SimpleDateFormat")
public class DeliveryItem implements Parcelable{
	public final static String EXTRA_KEY = "DeliveryItem";
	
	public final static String PROTO_DELIVERY_ORDER_ID = "_delivery_order_id";
	public final static String PROTO_NO_SPK = "no_spk";
	public final static String PROTO_SPK_DATE = "spk_date";
	public final static String PROTO_NO_SJ = "no_sj";
	public final static String PROTO_SJ_DATE = "sj_date";
	public final static String PROTO_KONSUMEN_NAME = "konsumen_name";
	public final static String PROTO_KONSUMEN_ADRS = "konsumen_adrs";
	public final static String PROTO_KONSUMEN_TELP = "konsumen_telp";
	public final static String PROTO_ID_TIPE_MOTOR = "id_tipe_motor";
	public final static String PROTO_JENIS_MOTOR = "jenis_motor";
	public final static String PROTO_WARNA_MOTOR = "warna_motor";
	public final static String PROTO_NO_MESIN = "no_mesin";
	public final static String PROTO_NO_RANGKA = "no_rangka";
	public final static String PROTO_TAHUN = "tahun";
	public final static String PROTO_LOCATION = "location";
	public final static String PROTO_LAT = "lat";
	public final static String PROTO_LNG = "lng";
	public final static String PROTO_AUTOGRAPH_TIMESTAMP = "autograph_timestamp";
	
	private final static String DATE_FORMAT_SOURCE = "yyyy-MM-dd hh:mm a";
	private final static String DATE_FORMAT = "yyyy-MM-dd HH:mm";
	
	public DeliveryItem() {}
	
	public DeliveryItem(String noSpk, String spkDate, String noSj,String sjDate,
			String konsumenName, String konsumenAdrs, String konsumenTelp,
			String idTipeMotor, String jenisMotor, String warnaMotor,
			String noMesin, String noRangka, String tahun, String autographPath,
			LatLng location, Date autographTimestamp) {
		
		assign(noSpk, spkDate, noSj,sjDate,
				konsumenName, konsumenAdrs, konsumenTelp,
				idTipeMotor, jenisMotor, warnaMotor,
				noMesin, noRangka, tahun, autographPath,
				location, autographTimestamp);
	}
	
	public void assign(String noSpk, String spkDate, String noSj,String sjDate,
			String konsumenName, String konsumenAdrs, String konsumenTelp,
			String idTipeMotor, String jenisMotor, String warnaMotor,
			String noMesin, String noRangka, String tahun, String autographPath,
			LatLng location, Date autographTimestamp) {
		
		SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT_SOURCE);
		
		this.noSpk = noSpk.trim();
		try {
			this.spkDate = format.parse(spkDate);
		} catch (ParseException e) {
			this.spkDate = null;
		}
		this.noSj = noSj.toUpperCase().trim();
		try {
			this.sjDate = format.parse(sjDate);
		} catch (ParseException e) {
			this.sjDate = null;
		}
		this.konsumenName = konsumenName.trim();
		this.konsumenAdrs = konsumenAdrs.trim();
		this.konsumenTelp = konsumenTelp.trim();
		this.idTipeMotor = idTipeMotor.trim();
		this.jenisMotor = jenisMotor.trim();
		this.warnaMotor = warnaMotor.trim();
		this.noMesin = noMesin.trim();
		this.noRangka = noRangka.trim();
		this.tahun = tahun.trim();
		this.autographPath = autographPath;
		this.location = location;
		this.autographTimestamp = autographTimestamp;
	}
	
	/**
	 * @return success
	 *   
	 */
	public boolean checkAutograph() {
		return Uri.parse(this.autographPath) != null && this.autographTimestamp != null;
	}
	
	public boolean checkPhoto() {
		return this.photos != null && this.photos.size() > 0;
	}
	
	public boolean checkLocation() {
		return this.location != null;
	}
	
	public void postRemote(int doId, PostCallback callback) {
		Context context = callback.getContext();
		RequestQueue requestQueue = VolleySingleton.getInstance(context);
		String url = PreferencesHelper.getOutgoingServer(context) + "/delivery_item";
		SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT);
		
		MultipartRequest request = new MultipartRequest(url, callback, new File(this.autographPath));
		request.put(PROTO_DELIVERY_ORDER_ID, String.valueOf(doId));
		request.put(PROTO_NO_SPK, this.getNoSpk());
		request.put(PROTO_SPK_DATE, this.getSpkDate());
		request.put(PROTO_NO_SJ, this.getNoSj());
		request.put(PROTO_SJ_DATE, this.getSjDate());
		request.put(PROTO_KONSUMEN_NAME, this.getKonsumenName());
		request.put(PROTO_KONSUMEN_ADRS, this.getKonsumenAdrs());
		request.put(PROTO_KONSUMEN_TELP, this.getKonsumenTelp());
		request.put(PROTO_ID_TIPE_MOTOR, this.getIdTipeMotor());
		request.put(PROTO_JENIS_MOTOR, this.getJenisMotor());
		request.put(PROTO_WARNA_MOTOR, this.getWarnaMotor());
		request.put(PROTO_NO_MESIN, this.getNoMesin());
		request.put(PROTO_NO_RANGKA, this.getNoRangka());
		request.put(PROTO_TAHUN, this.getTahun());
		request.put(PROTO_LAT, String.valueOf(this.getLocation().latitude));
		request.put(PROTO_LNG, String.valueOf(this.getLocation().longitude));
		request.put(PROTO_AUTOGRAPH_TIMESTAMP, df.format(this.autographTimestamp));
		
		request.setRetryPolicy(VolleySingleton.getDefaultRetryPolicy());
		requestQueue.add(request);
	}
	
	public static DeliveryItem parseJSON(JSONObject response) {
		if(response == null)
			return null;
		
		return new DeliveryItem(
				response.optString(DeliveryItem.PROTO_NO_SPK),
				response.optString(DeliveryItem.PROTO_SPK_DATE),
				response.optString(DeliveryItem.PROTO_NO_SJ),
				response.optString(DeliveryItem.PROTO_SJ_DATE),
				response.optString(DeliveryItem.PROTO_KONSUMEN_NAME),
				response.optString(DeliveryItem.PROTO_KONSUMEN_ADRS),
				response.optString(DeliveryItem.PROTO_KONSUMEN_TELP),
				response.optString(DeliveryItem.PROTO_ID_TIPE_MOTOR),
				response.optString(DeliveryItem.PROTO_JENIS_MOTOR),
				response.optString(DeliveryItem.PROTO_WARNA_MOTOR),
				response.optString(DeliveryItem.PROTO_NO_MESIN),
				response.optString(DeliveryItem.PROTO_NO_RANGKA),
				response.optString(DeliveryItem.PROTO_TAHUN),
				"",		// autograph
				null,	// location
				null);	// autographTimestamp
	}
	
	public void selectLocal(String noSj, SQLHelper sqlHelper) {
		if(sqlHelper == null)
			return;
		
		SQLiteDatabase db = sqlHelper.getReadableDatabase();
		String[] columns = {
			SQLSchemaHelper.DeliveryItemSQL.COLUMN_NO_SPK,
			SQLSchemaHelper.DeliveryItemSQL.COLUMN_SPK_DATE,
			SQLSchemaHelper.DeliveryItemSQL.COLUMN_NO_SJ,
			SQLSchemaHelper.DeliveryItemSQL.COLUMN_SJ_DATE,
			SQLSchemaHelper.DeliveryItemSQL.COLUMN_KONSUMEN_NAME,
			SQLSchemaHelper.DeliveryItemSQL.COLUMN_KONSUMEN_ADRS,
			SQLSchemaHelper.DeliveryItemSQL.COLUMN_KONSUMEN_TELP,
			SQLSchemaHelper.DeliveryItemSQL.COLUMN_ID_TIPE_MOTOR,
			SQLSchemaHelper.DeliveryItemSQL.COLUMN_JENIS_MOTOR,
			SQLSchemaHelper.DeliveryItemSQL.COLUMN_WARNA_MOTOR,
			SQLSchemaHelper.DeliveryItemSQL.COLUMN_NO_MESIN,
			SQLSchemaHelper.DeliveryItemSQL.COLUMN_NO_RANGKA,
			SQLSchemaHelper.DeliveryItemSQL.COLUMN_TAHUN,
			SQLSchemaHelper.DeliveryItemSQL.COLUMN_AUTOGRAPH,
			SQLSchemaHelper.DeliveryItemSQL.COLUMN_LATITUDE,
			SQLSchemaHelper.DeliveryItemSQL.COLUMN_LONGITUDE,
			SQLSchemaHelper.DeliveryItemSQL.COLUMN_AUTOGRAPH_TIMESTAMP
		};
		String[] selectionArgs = {noSj};
		
		Cursor c = db.query(SQLSchemaHelper.DeliveryItemSQL.TABLE, columns,
				SQLSchemaHelper.DeliveryItemSQL.COLUMN_NO_SJ + "=?", selectionArgs,
				null, null, null);
		
		if(c == null)
			return;
		
		SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT);
		if(c.moveToFirst()){
			Date timestamp;
			try {
				timestamp = c.getString(16) == null ? null : df.parse(c.getString(16));
			} catch (ParseException e) {
				timestamp = null;
			}
			
			assign(c.getString(0), c.getString(1), c.getString(2),
					c.getString(3), c.getString(4), c.getString(5), c.getString(6),
					c.getString(7), c.getString(8), c.getString(9), c.getString(10),
					c.getString(11), c.getString(12), c.getString(13),
					new LatLng(c.getDouble(14), c.getDouble(15)), timestamp);
			
			this.setPhotos(Photo.selectLocal(this.noSj, sqlHelper));
		}
		db.close();
	}
	
	public static DeliveryItem selecLocal(String noSj, SQLHelper sqlHelper) {
		DeliveryItem item = new DeliveryItem();
		item.selectLocal(noSj, sqlHelper);
		return item;
	}
	
	
	public static List<DeliveryItem> selectListLocal(String noDo, SQLHelper sqlHelper) {
		if(sqlHelper == null)
			return null;
		
		SQLiteDatabase db = sqlHelper.getReadableDatabase();
		String[] columns = {
			SQLSchemaHelper.DeliveryItemSQL.COLUMN_NO_SPK,
			SQLSchemaHelper.DeliveryItemSQL.COLUMN_SPK_DATE,
			SQLSchemaHelper.DeliveryItemSQL.COLUMN_NO_SJ,
			SQLSchemaHelper.DeliveryItemSQL.COLUMN_SJ_DATE,
			SQLSchemaHelper.DeliveryItemSQL.COLUMN_KONSUMEN_NAME,
			SQLSchemaHelper.DeliveryItemSQL.COLUMN_KONSUMEN_ADRS,
			SQLSchemaHelper.DeliveryItemSQL.COLUMN_KONSUMEN_TELP,
			SQLSchemaHelper.DeliveryItemSQL.COLUMN_ID_TIPE_MOTOR,
			SQLSchemaHelper.DeliveryItemSQL.COLUMN_JENIS_MOTOR,
			SQLSchemaHelper.DeliveryItemSQL.COLUMN_WARNA_MOTOR,
			SQLSchemaHelper.DeliveryItemSQL.COLUMN_NO_MESIN,
			SQLSchemaHelper.DeliveryItemSQL.COLUMN_NO_RANGKA,
			SQLSchemaHelper.DeliveryItemSQL.COLUMN_TAHUN,
			SQLSchemaHelper.DeliveryItemSQL.COLUMN_AUTOGRAPH,
			SQLSchemaHelper.DeliveryItemSQL.COLUMN_LATITUDE,
			SQLSchemaHelper.DeliveryItemSQL.COLUMN_LONGITUDE,
			SQLSchemaHelper.DeliveryItemSQL.COLUMN_AUTOGRAPH_TIMESTAMP
		};
		String[] selectionArgs = {noDo};
		
		Cursor c = db.query(SQLSchemaHelper.DeliveryItemSQL.TABLE, columns,
				SQLSchemaHelper.DeliveryItemSQL.COLUMN_NO_DO + "=?", selectionArgs,
				null, null, null);
		
		if(c == null)
			return null;
		
		List<DeliveryItem> items = new ArrayList<DeliveryItem>();
		SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT);
		while(c.moveToNext()) {
			Date timestamp;
			try {
				timestamp = c.getString(16) == null ? null : df.parse(c.getString(16));
			} catch (ParseException e) {
				timestamp = null;
			}
			
			DeliveryItem item = new DeliveryItem(c.getString(0), c.getString(1), c.getString(2),
					c.getString(3), c.getString(4), c.getString(5), c.getString(6),
					c.getString(7), c.getString(8), c.getString(9), c.getString(10),
					c.getString(11), c.getString(12), c.getString(13),
					new LatLng(c.getDouble(14), c.getDouble(15)), timestamp);
			
			item.setPhotos(Photo.selectLocal(item.getNoSj(), sqlHelper));
			items.add(item);
		}
		
		db.close();
		return items;
	}
	
	public void insertLocal(String noDo, SQLHelper sqlHelper) {
		SQLiteDatabase db = sqlHelper.getWritableDatabase();
		
		ContentValues values = new ContentValues();
		values.put(SQLSchemaHelper.DeliveryItemSQL.COLUMN_NO_DO, noDo);
		values.put(SQLSchemaHelper.DeliveryItemSQL.COLUMN_NO_SPK, this.getNoSpk());
		values.put(SQLSchemaHelper.DeliveryItemSQL.COLUMN_SPK_DATE, this.getSpkDate());
		values.put(SQLSchemaHelper.DeliveryItemSQL.COLUMN_NO_SJ, this.getNoSj());
		values.put(SQLSchemaHelper.DeliveryItemSQL.COLUMN_SJ_DATE, this.getSjDate());
		values.put(SQLSchemaHelper.DeliveryItemSQL.COLUMN_KONSUMEN_NAME, this.getKonsumenName());
		values.put(SQLSchemaHelper.DeliveryItemSQL.COLUMN_KONSUMEN_ADRS, this.getKonsumenAdrs());
		values.put(SQLSchemaHelper.DeliveryItemSQL.COLUMN_KONSUMEN_TELP, this.getKonsumenTelp());
		values.put(SQLSchemaHelper.DeliveryItemSQL.COLUMN_ID_TIPE_MOTOR, this.getIdTipeMotor());
		values.put(SQLSchemaHelper.DeliveryItemSQL.COLUMN_JENIS_MOTOR, this.getJenisMotor());
		values.put(SQLSchemaHelper.DeliveryItemSQL.COLUMN_WARNA_MOTOR, this.getWarnaMotor());
		values.put(SQLSchemaHelper.DeliveryItemSQL.COLUMN_NO_MESIN, this.getNoMesin());
		values.put(SQLSchemaHelper.DeliveryItemSQL.COLUMN_NO_RANGKA, this.getNoRangka());
		values.put(SQLSchemaHelper.DeliveryItemSQL.COLUMN_TAHUN, this.getTahun());
		values.put(SQLSchemaHelper.DeliveryItemSQL.COLUMN_AUTOGRAPH, this.getAutographPath());
		
		if(this.getLocation() != null) {
			values.put(SQLSchemaHelper.DeliveryItemSQL.COLUMN_LATITUDE, this.getLocation().latitude);
			values.put(SQLSchemaHelper.DeliveryItemSQL.COLUMN_LONGITUDE, this.getLocation().longitude);
		}
		
		db.insert(SQLSchemaHelper.DeliveryItemSQL.TABLE, null, values);
		db.close();
	}
	
	public boolean deleteLocal(SQLHelper sqlHelper) {
		SQLiteDatabase db = sqlHelper.getWritableDatabase();
		String[] whereArgs = {this.noSj};
		int row = db.delete(SQLSchemaHelper.DeliveryItemSQL.TABLE, SQLSchemaHelper.DeliveryItemSQL.COLUMN_NO_SJ + "=?", whereArgs);
		db.close();
		return row > 0;
	}
	
	public static void deleteAllLocal(SQLHelper sqlHelper) {
		SQLiteDatabase db = sqlHelper.getWritableDatabase();
		db.delete(SQLSchemaHelper.DeliveryItemSQL.TABLE, null, null);
		db.close();
	}
	
	public void updateLocal(SQLHelper sqlHelper) {
		SQLiteDatabase db = sqlHelper.getWritableDatabase();
		ContentValues values = new ContentValues();
		String[] whereArgs = {this.noSj};
		
		SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT);
		values.put(SQLSchemaHelper.DeliveryItemSQL.COLUMN_AUTOGRAPH, this.getAutographPath());
		values.put(SQLSchemaHelper.DeliveryItemSQL.COLUMN_LATITUDE, this.getLocation().latitude);
		values.put(SQLSchemaHelper.DeliveryItemSQL.COLUMN_LONGITUDE, this.getLocation().longitude);
		values.put(SQLSchemaHelper.DeliveryItemSQL.COLUMN_AUTOGRAPH_TIMESTAMP,df.format(this.autographTimestamp));
		
		db.update(SQLSchemaHelper.DeliveryItemSQL.TABLE, values,
				SQLSchemaHelper.DeliveryItemSQL.COLUMN_NO_SJ + "=?", whereArgs);
		db.close();
	}
	
	public void setAutographPath(String autographPath) {
		this.autographPath = autographPath;
	}
	
	public void setPhotos(List<Photo> photos) {
		this.photos = photos;
	}
	
	public void setLocation(LatLng location) {
		this.location = location;
	}

	public void setAutographTimestamp(Date autographTimestamp) {
		this.autographTimestamp = autographTimestamp;
	}

	public String getNoSpk() {
		return noSpk;
	}
	public String getSpkDate() {
		SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT_SOURCE);
		return format.format(spkDate);
	}
	public String getNoSj() {
		return noSj;
	}
	public String getSjDate() {
		SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT_SOURCE);
		return format.format(sjDate);
	}
	public String getKonsumenName() {
		return konsumenName;
	}
	public String getKonsumenAdrs() {
		return konsumenAdrs;
	}
	public String getKonsumenTelp() {
		return konsumenTelp;
	}
	public String getIdTipeMotor() {
		return idTipeMotor;
	}
	public String getJenisMotor() {
		return jenisMotor;
	}
	public String getWarnaMotor() {
		return warnaMotor;
	}
	public String getNoMesin() {
		return noMesin;
	}
	public String getNoRangka() {
		return noRangka;
	}
	public String getTahun() {
		return tahun;
	}
	
	public String getAutographPath() {
		return autographPath;
	}
	
	public List<Photo> getPhotos() {
		return photos;
	}
	
	public LatLng getLocation() {
		return location;
	}
	
	public Date getAutographTimestamp() {
		return autographTimestamp;
	}
	
	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel out, int flags) {
		out.writeString(noSpk);
		out.writeSerializable(spkDate);
		out.writeString(noSj);
		out.writeSerializable(sjDate);
		out.writeString(konsumenName);
		out.writeString(konsumenAdrs);
		out.writeString(konsumenTelp);
		out.writeString(idTipeMotor);
		out.writeString(jenisMotor);
		out.writeString(warnaMotor);
		out.writeString(noMesin);
		out.writeString(noRangka);
		out.writeString(tahun);
		out.writeString(autographPath);
		out.writeList(photos);
		out.writeParcelable(location, flags);
		out.writeSerializable(autographTimestamp);
	}
	
	public static final Parcelable.Creator<DeliveryItem> CREATOR
	    	= new Parcelable.Creator<DeliveryItem>() {
		public DeliveryItem createFromParcel(Parcel in) {
		    return new DeliveryItem(in);
		}
		
		public DeliveryItem[] newArray(int size) {
		    return new DeliveryItem[size];
		}
	};
	
	private DeliveryItem(Parcel in) {
		noSpk = in.readString();
		spkDate = (Date) in.readSerializable();
		noSj = in.readString();
		sjDate = (Date) in.readSerializable();
		konsumenName = in.readString();
		konsumenAdrs = in.readString();
		konsumenTelp = in.readString();
		idTipeMotor = in.readString();
		jenisMotor = in.readString();
		warnaMotor = in.readString();
		noMesin = in.readString();
		noRangka = in.readString();
		tahun = in.readString();
		autographPath = in.readString();
		photos = new ArrayList<Photo>(); in.readList(photos, Photo.class.getClassLoader());
		location = in.readParcelable(LatLng.class.getClassLoader());
		autographTimestamp = (Date) in.readSerializable();
	}

	private String noSpk;
	private Date spkDate;
	private String noSj;
	private Date sjDate;
	private String konsumenName;
	private String konsumenAdrs;
	private String konsumenTelp;
	private String idTipeMotor;
	private String jenisMotor;
	private String warnaMotor;
	private String noMesin;
	private String noRangka;
	private String tahun;
	private String autographPath;
	private List<Photo> photos;
	private LatLng location;
	private Date autographTimestamp;
	
	public static abstract class PostCallback implements MultipartRequest.Listener {
		public abstract void onSuccess(int id);
		public abstract void onError(String errMessage);
		
		public PostCallback(Context context) {
			this.mContext = context;
		}
		
		@Override
		public void onResponse(JSONObject response) {
			String msg = response.optString("msg");
			if(msg.equals("success"))
				onSuccess(response.optInt("id"));
			else if(msg.equals("error"))
				onError(response.optString("err"));
		}

		@Override
		public Context getContext() {
			return mContext;
		}
		
		private Context mContext;
	}
}
