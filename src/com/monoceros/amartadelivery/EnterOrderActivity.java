package com.monoceros.amartadelivery;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.monoceros.amartadelivery.helpers.SQLHelper;
import com.monoceros.amartadelivery.models.DeliveryItem;
import com.monoceros.amartadelivery.models.DeliveryOrder;
import com.monoceros.amartadelivery.models.Photo;

public class EnterOrderActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_enter_order);
		
		DeliveryOrder order = getIntent().getParcelableExtra(DeliveryOrder.EXTRA_NAME);
		if(order != null) {
			getActionBar().setHomeButtonEnabled(true);
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
			
		if (savedInstanceState == null) {
			PlaceholderFragment fragment = new PlaceholderFragment();
			Bundle args = new Bundle();
			args.putParcelable(DeliveryOrder.EXTRA_NAME, order);
			fragment.setArguments(args);
			getFragmentManager().beginTransaction()
					.add(R.id.container, fragment).commit();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.enter_order, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			Intent intent = new Intent(this, EnterIpActivity.class);
			startActivity(intent);
			return true;
		} else if(id == android.R.id.home) {
			onBackPressed();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment implements OnClickListener {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_enter_order,
					container, false);
			
			mButtonOk = (Button) rootView.findViewById(R.id.buttonOk);
			mEditOrderNumber = (EditText) rootView.findViewById(R.id.editOrderNumber);
			
			mButtonOk.setOnClickListener(this);
			return rootView;
		}

		@Override
		public void onClick(View v) {
			DeliveryOrder.getRemote(mEditOrderNumber.getText().toString(), new DeliveryOrder.GetRemoteCallback(getActivity()) {
				@Override
				public void onErrorResponse(VolleyError error) {
					Toast.makeText(getContext(), "Connection error", Toast.LENGTH_SHORT).show();
				}

				@Override
				public void onResponse(DeliveryOrder result) {
					SQLHelper sqlHelper = new SQLHelper(getActivity());
					DeliveryOrder order = getArguments().getParcelable(DeliveryOrder.EXTRA_NAME);
					if(order != null) {
						DeliveryOrder.deleteAllLocal(sqlHelper);
						DeliveryItem.deleteAllLocal(sqlHelper);
						Photo.deleteAllLocal(sqlHelper);
					}
					result.insertLocal(sqlHelper);
					
					Intent intent = new Intent(getActivity(), DeliveryItemListActivity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
					intent.putExtra(DeliveryOrder.EXTRA_NAME, result);
					startActivity(intent);
					getActivity().finish();
					
					sqlHelper.close();
				}
				
			});
		}
		
		private EditText mEditOrderNumber;
		private Button mButtonOk;
	}
}
