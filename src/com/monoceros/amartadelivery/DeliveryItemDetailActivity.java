package com.monoceros.amartadelivery;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.app.AlertDialog;
import android.app.ListFragment;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.location.LocationClient;
import com.google.android.gms.maps.model.LatLng;
import com.monoceros.amartadelivery.adapters.DeliveryItemDetailAdapter;
import com.monoceros.amartadelivery.helpers.BitmapWorker;
import com.monoceros.amartadelivery.helpers.SQLHelper;
import com.monoceros.amartadelivery.models.DeliveryItem;
import com.monoceros.amartadelivery.models.Photo;
import com.monoceros.locationactivity.LocationActivity;

public class DeliveryItemDetailActivity extends LocationActivity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_delivery_item_detail);
		
		getActionBar().setHomeButtonEnabled(true);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		DeliveryItem item = getIntent().getParcelableExtra(DeliveryItem.EXTRA_KEY);
		if (savedInstanceState == null) {
			Bundle args = new Bundle();
			args.putParcelable(DeliveryItem.EXTRA_KEY, item);
			
			mFragment = new PlaceholderFragment(getLocationClient());
			mFragment.setArguments(args);
			getFragmentManager().beginTransaction()
					.add(R.id.container, mFragment).commit();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.delivery_item_detail, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		if(item.getItemId() == android.R.id.home) {
			onBackPressed();
			return true;
		}
		
		return mFragment.onOptionsItemSelected(item);		
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends ListFragment {
		private static int REQUEST_CAMERA = 0;
		
		private LocationClient mLocationClient;
		
		public PlaceholderFragment(LocationClient locationClient) {
			mLocationClient = locationClient;
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(
					R.layout.fragment_delivery_item_detail, container, false);
			return rootView;
		}
		
		@Override
		public boolean onOptionsItemSelected(MenuItem item) {
			int id = item.getItemId();
			if (id == R.id.action_camera) {
				dispatchTakePictureIntent();
				return true;
			} else if(id == R.id.action_sign) {
				displayCanvas();
				return true;
			}
			return super.onOptionsItemSelected(item);
		}
		
		@Override
		public void onViewCreated(View view, Bundle savedInstanceState) {
			super.onViewCreated(view, savedInstanceState);
			
			mItem = getArguments().getParcelable(DeliveryItem.EXTRA_KEY);
			mThumnbnailWidth = getResources().getDimensionPixelSize(R.dimen.thumbnail_width);
	    	mThumbnailHeight = getResources().getDimensionPixelSize(R.dimen.thumbnail_height);
	    	
			List<ListItemDetail> list = new ArrayList<ListItemDetail>();
			addToListItemDetail(R.string.no_spk, mItem.getNoSpk(), list);
			addToListItemDetail(R.string.spk_date, mItem.getSpkDate(), list);
			addToListItemDetail(R.string.no_sj, mItem.getNoSj(), list);
			addToListItemDetail(R.string.sj_date, mItem.getSpkDate(), list);
			addToListItemDetail(R.string.konsumen_name, mItem.getKonsumenName(), list);
			addToListItemDetail(R.string.konsumen_adrs, mItem.getKonsumenAdrs(), list);
			addToListItemDetail(R.string.konsumen_telp, mItem.getKonsumenTelp(), list);
			addToListItemDetail(R.string.id_tipe_motor, mItem.getIdTipeMotor(), list);
			addToListItemDetail(R.string.jenis_motor, mItem.getJenisMotor(), list);
			addToListItemDetail(R.string.warna_motor, mItem.getWarnaMotor(), list);
			addToListItemDetail(R.string.no_mesin, mItem.getNoMesin(), list);
			addToListItemDetail(R.string.no_rangka, mItem.getNoRangka(), list);
			addToListItemDetail(R.string.tahun, mItem.getTahun(), list);
			
			DeliveryItemDetailAdapter adapter = new DeliveryItemDetailAdapter(getActivity(), R.layout.cell_delivery_item_detail, list);
			setListAdapter(adapter);
			
			View gallery = (View) getActivity().getLayoutInflater().inflate(R.layout.layout_gallery, null);
			mLayoutThumbnail = (LinearLayout) gallery.findViewById(R.id.layoutThumbnail);
			
			getListView().addFooterView(gallery);
			
			// re-retrieve photo list from database
			SQLHelper sqlHelper = new SQLHelper(getActivity());
			mItem.setPhotos(Photo.selectLocal(mItem.getNoSj(), sqlHelper));
			sqlHelper.close();
			
			for(int i = 0, size = mItem.getPhotos().size(); i < size; i++) {
				Uri uri = Uri.parse(mItem.getPhotos().get(i).getPhotoPath());
				displayGallery(uri, false);
			}
		}
		
		private void addToListItemDetail(int attributeResId, String value, List<ListItemDetail> list) {
			ListItemDetail listItem = new ListItemDetail();
			listItem.attributeId = attributeResId;
			listItem.attribute = getString(attributeResId);
			listItem.value = value;
			list.add(listItem);
		}
		
		@Override
		public void onActivityResult(int requestCode, int resultCode,
				Intent data) {
			if(resultCode == RESULT_OK) {
				if(requestCode == REQUEST_CAMERA) {
					displayGallery(this.mCurrentPhotoPath, true);
					
				}
			}
		}
		
		private void displayGallery(final Uri uri, final boolean addToDatabase) {
			BitmapWorker bw = new BitmapWorker(uri) {
				@Override
				protected void onPostExecute(Bitmap bitmap) {
					displayThumbnailInGallery(uri, bitmap, addToDatabase);
				}
			};
			
			bw.execute(mThumnbnailWidth, mThumbnailHeight);
			
			Intent mediaScanIntent = new Intent( Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
			mediaScanIntent.setData(uri);
			getActivity().sendBroadcast(mediaScanIntent);
		}
		
		private void displayThumbnailInGallery(Uri uri, Bitmap bm, boolean addToDatabase) {
			LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(mThumnbnailWidth, mThumbnailHeight);
	    	lp.setMargins(8,8,8,8);
	    	
			ImageView thumbnail = new ImageView(getActivity());
			thumbnail.setLayoutParams(lp);
			thumbnail.setBackgroundColor(getResources().getColor(android.R.color.darker_gray));
			thumbnail.setImageResource(android.R.drawable.ic_menu_gallery);
			thumbnail.setImageBitmap(bm);
			thumbnail.setScaleType(ScaleType.CENTER_CROP);
			thumbnail.setTag(uri);
			thumbnail.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent intent = new Intent(getActivity(), FullscreenPhotoActivity.class);
					intent.putExtra(FullscreenPhotoActivity.EXTRA_IMAGE_PATH, ((Uri)v.getTag()).getPath());
					intent.putExtra(FullscreenPhotoActivity.EXTRA_NO_SJ, mItem.getNoSj());
					startActivity(intent);
				}
			});
			thumbnail.setOnLongClickListener(new OnLongClickListener() {
				@Override
				public boolean onLongClick(final View v) {
					new AlertDialog.Builder(getActivity())
						.setTitle(getString(R.string.confirmation))
						.setMessage(getString(R.string.delete_photo))
						.setIcon(android.R.drawable.ic_dialog_alert)
						.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
						    @Override
							public void onClick(DialogInterface dialog, int whichButton) {
						    	SQLHelper sqlHelper = new SQLHelper(getActivity());
						    	boolean deleted = Photo.deleteLocal(((Uri)v.getTag()).getPath(), sqlHelper);
						    	if(deleted)
						    		mLayoutThumbnail.removeView(v);
						    	else
						    		Toast.makeText(getActivity(),
						    				getResources().getString(R.string.error_delete_photo),
						    				Toast.LENGTH_SHORT).show();
						    }
						})
						.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) { }
						})
						.setOnCancelListener(new OnCancelListener() {
							@Override
							public void onCancel(DialogInterface dialog) { }
						})
						.show();					
					
					return true;
				}
			});
			mLayoutThumbnail.addView(thumbnail);
			
			if(addToDatabase) {
				LatLng lastLocation = new LatLng(mLocationClient.getLastLocation().getLatitude(),
						mLocationClient.getLastLocation().getLongitude());
				
				Photo photo = new Photo(mCurrentPhotoPath.getPath(),lastLocation, null);
				SQLHelper sqlHelper = new SQLHelper(getActivity());
				photo.insertLocal(mItem.getNoSj(), sqlHelper);
				sqlHelper.close();
			}
		}
		
		private void displayCanvas() {
			Intent intent = new Intent(getActivity(), AutographCanvasActivity.class);
			intent.putExtra(DeliveryItem.EXTRA_KEY, mItem);
			startActivity(intent);
		}
		
		private void dispatchTakePictureIntent() {
		    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		    // Ensure that there's a camera activity to handle the intent
		    if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
		        // Create the File where the photo should go
		        File photoFile = null;
		        try {
		            photoFile = createImageFile();
		        } catch (IOException ex) {
		            Toast.makeText(getActivity(), "No write access", Toast.LENGTH_SHORT).show();
		        }
		        // Continue only if the File was successfully created
		        if (photoFile != null) {
		            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
		                    Uri.fromFile(photoFile));
		            startActivityForResult(takePictureIntent, REQUEST_CAMERA);
		        }
		    }
		}
		
		private File createImageFile() throws IOException {
		    // Create an image file name	
		    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
		    String imageFileName = "IMG_" + timeStamp + ".jpg";
		    File storageDir = Environment.getExternalStoragePublicDirectory(
		    		Environment.DIRECTORY_DCIM);
		    
		    File image = new File(storageDir.getPath() + File.separator + imageFileName);

		    mCurrentPhotoPath = Uri.fromFile(image);
		    return image;
		}
		
		private DeliveryItem mItem;
		private Uri mCurrentPhotoPath;
		private LinearLayout mLayoutThumbnail;
		int mThumnbnailWidth;
    	int mThumbnailHeight;
	}
	
	private PlaceholderFragment mFragment;
	
	public static class ListItemDetail {
		public int attributeId;
		public String attribute;
		public String value;
	}
}
